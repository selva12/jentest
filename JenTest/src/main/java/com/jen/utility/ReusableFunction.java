package com.jen.utility;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ReusableFunction {
	WebDriver driver;
	public  WebDriver chrome()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\USER\\Downloads\\Selenium\\chromedriver.exe");
		driver=new ChromeDriver();
	//System.setProperty("webdriver.gecko.driver","C:\\Users\\USER\\Downloads\\Selenium\\geckodriver.exe");
	//WebDriver driver = new FirefoxDriver();
			
driver.manage().window().maximize();
return driver;
	}
	

	public By getBy(WebDriver driver, String element){
		String[] value=element.split(">");
		By by=null;
		
		switch(value[0]){
		
		case "id":
			by =By.id(value[1]);
			break; 
		case "name":
			by=By.name(value[1]);
			break;
		case "tagname":
			by=By.tagName(value[1]);
			break;
		case "partialtext":
			by=By.partialLinkText(value[1]);
			break;
		case "xpath":
			by=By.xpath(value[1]);
			break;
		case "cssselector":
			by=By.cssSelector(value[1]);
			break;
			
		case "linkText":
			by=By.linkText(value[1]);
			break;
		case "className":
			by=By.className(value[1]);
			break;
			
		
		}
		return by;
	
		
	}
	public void clickButton(WebDriver driver, String element) {
			try{
				
				
			driver.findElement(getBy(driver,element)).click();
				}catch(Exception e){
				System.out.println("Errir in button "+element);
			}}
	
	
	public void sendValue(WebDriver driver, String element, String value){
		try {
			//To check enable
			//To check visible
			//Highlight
			
		driver.findElement(getBy(driver,element)).sendKeys(value);
	}catch(Exception e){
		
	}
		
}
	

	public void selectedby(WebDriver driver, String element,String data)
	{
		Select dropdown=new Select(driver.findElement(getBy(driver,element)));
		dropdown.selectByVisibleText(data);

	}
public void setClipboardData(String value)throws Exception
{
	StringSelection stringsel = new StringSelection(value);
	 
	  Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringsel,null);
	 System.out.println("selection" +stringsel);


	 Robot robot = new Robot();
	 Thread.sleep(1000);
	      
	  // Press Enter
	 robot.keyPress(KeyEvent.VK_ENTER);
	 
	// Release Enter
	 robot.keyRelease(KeyEvent.VK_ENTER);
	 
	  // Press CTRL+V
	 robot.keyPress(KeyEvent.VK_CONTROL);
	 robot.keyPress(KeyEvent.VK_V);
	 
	// Release CTRL+V
	 robot.keyRelease(KeyEvent.VK_CONTROL);
	 robot.keyRelease(KeyEvent.VK_V);
	 Thread.sleep(1000);
	        
	         
	 robot.keyPress(KeyEvent.VK_ENTER);
	 robot.keyRelease(KeyEvent.VK_ENTER);	
		
	}
public void winHandle()
{
	String hand=driver.getWindowHandle();
	Set<String>hands=driver.getWindowHandles();
	String winhand=hands.iterator().next();
	if(winhand !=hand)
	{
		String secwind=winhand;
		driver.switchTo().window(secwind);
	}
	
}
 


}
